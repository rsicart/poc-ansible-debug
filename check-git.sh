#!/Usr/bin/env bash

function git_check() {
    for branch_name in master main; do
        if [ `git rev-parse --verify $branch_name 2>/dev/null` ]
        then
            BRANCH=$branch_name
        fi
    done

    if [ -z "$BRANCH" ]
    then
        echo "It looks like you don't have a branch named master or main in your git repository"
        exit 1
    fi

    git checkout $BRANCH > /dev/null 2>&1

    git status | grep "nothing to commit, working tree clean" > /dev/null

    if [ $? -ne 0 ]; then
        echo -e "\033[0;31m!\033[0m You have uncommited changes"
        git add --all
        echo -e "\n\033[0;31m>\033[0m Commit message :"; read MESSAGE
        echo 'git commit -m "$MESSAGE"'
        echo 'git push'
    else
        # push already commited changes (issue #4)
        git status | grep "Your branch is ahead of " > /dev/null && echo 'git push'

        echo -e "\033[0;31m>\033[0m Updating codebase"
        echo 'git remote update && git reset --hard origin/$BRANCH && git clean -f'
    fi
}

git_check
